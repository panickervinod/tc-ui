import ReactDOM from 'react-dom'
import router from './src/example/Router.jsx'

ReactDOM.render(router(), document.getElementById('root'))